const e = require("express");

// Subscriber Log to check maximum joiners
exports.subscriber_log = function (req, res) {
    const contractAddress = req.params.trustedLendingCircleAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'subscriberLog',
            { fromBlock: 0, toBlock: 'latest' },
            (err, events) => {
                if (events.length < req.params.maxTicket) {
                    return res.render('subscriber/joinfund/joinFund', {
                        fundName: req.params.fundName,
                        fundInstallment: req.params.fundInstallment,
                        fundAddress: req.params.trustedLendingCircleAddress
                    });
                } else {
                    return res.render('subscriber/joinfund/joinFundFull');
                }
            });
    } else {
        res.redirect('/login');
    }
}

// subscriber log for foreman
exports.subscriber_log_foreman = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'subscriberLog',
            { fromBlock: 0, toBlock: 'latest' },
            (err, events) => {
                subscriberLogArray = [];
                if (events.length == 0) {
                    return res.json({ 'data': subscriberLogArray });
                } else {
                    for (var i = 0; i < events.length; i++) {
                        subscriberLogArray.push({
                            installmentIndex: events[i].returnValues[0],
                            ticketNumber: events[i].returnValues[1],
                            subscriberAddress: events[i].returnValues[2],
                            pay: events[i].returnValues[3],
                            fine: events[i].returnValues[4],
                            deposit: events[i].returnValues[5],
                            balance: events[i].returnValues[6],
                            winner: events[i].returnValues[7],
                            subscriber: events[i].returnValues[8],
                            due: events[i].returnValues[9],
                            status: events[i].returnValues[10]
                        });
                    }
                    return res.json({ 'data': subscriberLogArray });
                }
            });
    } else {
        res.redirect('/login');
    }
}

// Balance Log for foreman
exports.balance_log_foreman = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'balanceLog',
            { fromBlock: 0, toBlock: 'latest' },
            (err, events) => {
                balanceLogArray = [];
                if (events.length == 0) {
                    return res.json({ 'data': balanceLogArray });
                } else {
                    for (var i = 0; i < events.length; i++) {
                        balanceLogArray.push({
                            time: events[i].returnValues[0],
                            subscriberAddress: events[i].returnValues[1],
                            credit: events[i].returnValues[2],
                            debit: events[i].returnValues[3],
                            balance: events[i].returnValues[4]
                        });
                    }
                    return res.json({ 'data': balanceLogArray });
                }
            });
    } else {
        res.redirect('/login');
    }
}

// Installment Log for foreman
exports.installment_log_foreman = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'installmentLog',
            { fromBlock: 0, toBlock: 'latest' },
            (err, events) => {
                installmentLogArray = [];
                if (events.length == 0) {
                    return res.json({ 'data': installmentLogArray });
                } else {
                    for (var i = 0; i < events.length; i++) {
                        installmentLogArray.push({
                            index: events[i].returnValues[0],
                            Pay: events[i].returnValues[1],
                            fundClosed: events[i].returnValues[2],
                            fundCanceled: events[i].returnValues[3]
                        });
                    }
                    return res.json({ 'data': installmentLogArray });
                }
            });
    } else {
        res.redirect('/login');
    }
}

/* update installment index by foreman*/
exports.update_installment_index = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.methods.updateInstallmentIndex()
            .send({ from: req.query.foremanAccount })
            .then((result) => {
                res.status(200).json({ message: 'success' });
            })
            .catch(err => {
                res.json({ message: 'error' });
            })
    } else {
        res.redirect('/login');
    }
}

exports.subscriber_log_for_specific_subscriber = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const subscriberAddress = req.query.subscriberAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'subscriberLog',
            {
                filter: { subscriberAddress: subscriberAddress },
                fromBlock: 0,
                toBlock: 'latest'
            },
            (err, events) => {
                subscriberLogArray = [];
                pay = 0;
                fine = 0;
                totalToPay = 0;
                if (events.length == 0) {
                    return res.json({ 'data': subscriberLogArray, 'pay': pay, 'fine': fine, 'totalToPay': totalToPay });
                } else {
                    pay = events[0].returnValues[3];
                    fine = events[0].returnValues[4];
                    totalToPay = Number(events[0].returnValues[3]) + Number(events[0].returnValues[4]);
                    for (var i = 0; i < events.length; i++) {
                        subscriberLogArray.push({
                            installmentIndex: events[i].returnValues[0],
                            ticketNumber: events[i].returnValues[1],
                            subscriberAddress: events[i].returnValues[2],
                            pay: events[i].returnValues[3],
                            fine: events[i].returnValues[4],
                            deposit: events[i].returnValues[5],
                            balance: events[i].returnValues[6],
                            winner: events[i].returnValues[7],
                            subscriber: events[i].returnValues[8],
                            due: events[i].returnValues[9],
                            status: events[i].returnValues[10]
                        });
                    }
                    return res.json({ 'data': subscriberLogArray, 'pay': pay, 'fine': fine, 'totalToPay': totalToPay });
                }
            });
    } else {
        res.redirect('/login');
    }
}

exports.auction_log_event = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);
    if (req.isAuthenticated()) {
        auctionLogArray = [];
        TrustedLendingCircleContract.getPastEvents(
            'auctionLog',
            {
                fromBlock: 0,
                toBlock: 'latest'
            },
            (err, events) => {
                if (events.length == 0) {
                    //return res.json({ 'data': auctionLogArray});
                } else {
                    for (var i = 0; i < events.length; i++) {
                        auctionLogArray.push({
                            installmentIndex: events[i].returnValues[0],
                            bidStart: events[i].returnValues[1],
                            bidEnd: events[i].returnValues[2]
                        });
                    }
                    // return res.json({ 'data': auctionLogArray});
                }
            });
        TrustedLendingCircleContract.getPastEvents(
            'winnerLog',
            {
                fromBlock: 0,
                toBlock: 'latest'
            },
            (err, events) => {
                if (events.length == 0) {
                    //return res.json({ 'data': auctionLogArray});
                } else {
                    for (var i = 0; i < events.length; i++) {
                        auctionLogArray[i]['winTimeStamp'] = events[i].returnValues[1];
                        auctionLogArray[i]['highestBid'] = events[i].returnValues[2];
                        auctionLogArray[i]['winnerAddress'] = events[i].returnValues[3];
                    }
                    //return res.json({ 'data': auctionLogArray});
                }
                return res.json({ 'data': auctionLogArray });
            });
    } else {
        res.redirect('/login');
    }
}

exports.auction_event_log_event = function (req, res) {
    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'auctionEventLog',
            {
                fromBlock: 0,
                toBlock: 'latest'
            },
            (err, events) => {
                auctionLogArray = [];
                if (events.length == 0) {
                    return res.json({ 'data': auctionLogArray });
                } else {
                    for (var i = 0; i < events.length; i++) {
                        auctionLogArray.push({
                            installmentIndex: events[i].returnValues[0],
                            timeStamp: events[i].returnValues[1],
                            highestBid: events[i].returnValues[2],
                            tickets: events[i].returnValues[3]
                        });
                    }
                    return res.json({ 'data': auctionLogArray });
                }
            });
    } else {
        res.redirect('/login');
    }
}

exports.balance_log_for_specific_subscriber = function (req, res) {
    const contractAddress = req.query.contractAddress;
    const subscriberAddress = req.query.subscriberAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);
    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'balanceLog',
            {
                filter: { memberAddress: subscriberAddress },
                fromBlock: 0,
                toBlock: 'latest'
            },
            (err, events) => {
                balanceLogArray = [];
                creditTotal = 0;
                debitTotal = 0;
                balanceTotal = 0;
                if (events.length == 0) {
                    return res.json({ 'data': balanceLogArray });
                } else {
                    for (var i = 0; i < events.length; i++) {
                        creditTotal += parseInt(events[i].returnValues[2]);
                        debitTotal += parseInt(events[i].returnValues[3]);
                        balanceLogArray.push({
                            time: events[i].returnValues[0],
                            credit: events[i].returnValues[2],
                            debit: events[i].returnValues[3],
                            balance: creditTotal - debitTotal
                        });
                    }
                    return res.json({ 'data': balanceLogArray });
                }
            });
    } else {
        res.redirect('/login');
    }
}

exports.verify_log_events = async function (req, res) {
    const contractAddress = req.query.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);

    if (req.isAuthenticated()) {
        TrustedLendingCircleContract.getPastEvents(
            'subscriberLog',
            { fromBlock: 0, toBlock: 'latest' },
            (err, events) => {
                subscriber = [];
                subscriberLogArray = [];
                if (events.length == 0) {
                    return res.json({ 'data': subscriberLogArray });
                } else {
                    for (var i = 0; i < events.length; i++) {
                        var subscriberAddress = events[i].returnValues[2];
                        subscriber[subscriberAddress] = {
                            installmentIndex: events[i].returnValues[0],
                            ticketNumber: events[i].returnValues[1],
                            subscriberAddress: events[i].returnValues[2],
                            pay: events[i].returnValues[3],
                            fine: events[i].returnValues[4],
                            deposit: events[i].returnValues[5],
                            balance: events[i].returnValues[6],
                            winner: events[i].returnValues[7],
                            subscriber: events[i].returnValues[8],
                            due: events[i].returnValues[9],
                            status: events[i].returnValues[10]
                        };
                    }
                    for (const [key, value] of Object.entries(subscriber)) {
                        subscriberLogArray.push(value);
                    }

                    return res.json({ 'data':  subscriberLogArray});
                }
            });
    } else {
        res.redirect('/login');
    }
}


exports.subscriberLog_specific = function(req,res) {
    
};

