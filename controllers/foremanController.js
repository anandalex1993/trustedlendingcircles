const { default: Web3 } = require("web3");

// Fund Log for All Foreman
exports.fund_log_foreman_all = function(req,res) {
    if (req.isAuthenticated()) {
        ForemanContract.getPastEvents('fundLog',
        { fromBlock: 0, toBlock: 'latest'}, (err, events) => {
            eventArr = [];
            for (var i=0; i<= (events.length-1); i++) {
                if (events[i].returnValues[6] == 1 ) {
                    eventArr.push({
                        fundName: events[i].returnValues[1],
                        installmentAmount: events[i].returnValues[4],
                        noOfInstallments: events[i].returnValues[5],
                        fundAddress: events[i].returnValues[2],
                        status: events[i].returnValues[6]
                    });
                }
                if (eventArr.length == events.length) {
                    return res.render('foreman/foremanHome', {title: 'Home123', events: eventArr});
                }
            }
            res.render('foreman/foremanHome', {events: eventArr});
        });
    } else {
        res.redirect('/login');
    }
}

// Fund Log Created by Foreman
exports.fund_log_foreman_my = function(req,res) {
    if (req.isAuthenticated()) {
        ForemanContract.getPastEvents('fundLog',
        {
            filter: {foreman: req.query.account},
            fromBlock: 0,
            toBlock: 'latest'
        }, (err, events) => {
            eventArr = [];
            for (var i=0; i<= (events.length-1); i++) {
                eventArr.push({
                    fundName: events[i].returnValues[1],
                    installmentAmount: events[i].returnValues[4],
                    noOfInstallments: events[i].returnValues[5],
                    fundAddress: events[i].returnValues[2]
                });

                if (eventArr.length == events.length) {
                    return res.json({events: eventArr});
                }
            }
            res.json({events: eventArr});
        });
    } else {
        res.redirect('/login');
    }
}

// Fund Log for Subscriber
exports.fund_log_subscriber = function(req,res) {
    ssn = req.session;
    if (req.isAuthenticated()) {
        ForemanContract.getPastEvents('fundLog', {fromBlock: 0, toBlock: 'latest'}, (err, events) => {
            eventArr = [];
            for (var i=0; i<= (events.length-1); i++) {
                eventArr.push({
                    fundName: events[i].returnValues[1],
                    installmentAmount: events[i].returnValues[4],
                    noOfInstallments: events[i].returnValues[5],
                    fundAddress: events[i].returnValues[2]
                });

                if (eventArr.length == events.length) {
                    return res.render('subscriber/subscriberHome', {events: eventArr});
                }
            }
            res.render('subscriber/subscriberHome', {events: eventArr});
        });
    } else {
        res.redirect('/login');
    }
}
