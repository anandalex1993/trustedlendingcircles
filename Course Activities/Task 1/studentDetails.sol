//Create a solidity smart contract for CED B5 students

// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

contract StudentDetails {
    enum statusOfEachPhase {Not_Submitted, Submitted}

    struct Student {
        uint256 id; //student id
        string name; //Student Name
        statusOfEachPhase submissionStatus; //Submission Status
        string url; //Gitlab URL
        string projectName; //Project Name
        string projectDescription; //Project Description
    }

    mapping(uint256 => Student) private students;

    //function to store details
    function store(
        uint256 _id,
        string memory _name,
        string memory _url,
        string memory _projectName,
        string memory _projectDescription,
        uint256 submissionStatus
    ) public {
        require(!(students[_id].id == _id), "Student already exists!");

        students[_id].id = _id;
        students[_id].name = _name;
        students[_id].url = _url;
        students[_id].projectName = _projectName;
        students[_id].projectDescription = _projectDescription;
        submissionStatus == 1
            ? students[_id].submissionStatus = statusOfEachPhase.Submitted
            : students[_id].submissionStatus = statusOfEachPhase.Not_Submitted;
    }

    //function to update details
    function update(
        uint256 _id,
        string memory _name,
        string memory _url,
        string memory _projectName,
        string memory _projectDescription,
        uint256 status
    ) public {
        require(students[_id].id == _id, "No student with this Id");

        bytes memory tempName = bytes(_name);
        if (tempName.length > 0) {
            students[_id].name = _name;
        }

        bytes memory tempUrl = bytes(_url);
        if (tempUrl.length > 0) {
            students[_id].url = _url;
        }

        bytes memory tempProjectName = bytes(_projectName);
        if (tempProjectName.length > 0) {
            students[_id].projectName = _projectName;
        }

        bytes memory tempProjectDescription = bytes(_projectDescription);
        if (tempProjectDescription.length > 0) {
            students[_id].projectDescription = _projectDescription;
        }

        if (status == 1) {
            students[_id].submissionStatus = statusOfEachPhase.Submitted;
        } else {
            students[_id].submissionStatus = statusOfEachPhase.Not_Submitted;
        }
    }

    //function to retrieve details
    function retrieve(uint256 _id)
        public
        view
        returns (
            uint256,
            string memory,
            string memory,
            string memory,
            string memory,
            string memory
        )
    {
        require(students[_id].id == _id, "No student with this Id");
        string memory statusString;
        if (uint8(students[_id].submissionStatus) == 1) {
            statusString = "Submitted";
        } else {
            statusString = "Not Submitted";
        }
        return (
            students[_id].id,
            students[_id].name,
            students[_id].url,
            students[_id].projectName,
            students[_id].projectDescription,
            statusString
        );
    }
}
