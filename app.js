require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var $ = require('jquery');

const session = require('express-session');
const passport = require('passport');
Web3 = require('web3');

ForemanContractJSON = require(path.join(__dirname, 'Ethereum/build/contracts/Foreman.json'));
TrustedLendingCircleContractJSON = require(path.join(__dirname, 'Ethereum/build/contracts/TrustedLendingCircle.json'));
const HDWalletProvider = require('@truffle/hdwallet-provider');
//infura project_id
const infuraKey = "c92be5ada61c4ad0aa314535915cc82c";
//12 word mnemonic which addresses are created from.
const  mnemonic = "voyage explain farm toast work audit odor economy hidden gaze monkey talent";
//it will tell the provider to manage the address at the index specified
const address_index= 0;
//it will create number addresses when instantiated
const num_addresses= 5;
//web3 provider

var homeRouter = require('./routes/home');
var foremanRouter = require('./routes/foreman');
var subscriberRouter = require('./routes/subscriber');

// const provider = new HDWalletProvider(mnemonic, `https://ropsten.infura.io/v3/${infuraKey}`, address_index, num_addresses);
// web3 = new Web3(provider);
// contractAddress = ForemanContractJSON.networks['3'].address;
// var contractAbi = ForemanContractJSON.abi;

web3 = new Web3('http://localhost:7545');
const contractAddress = ForemanContractJSON.networks['5777'].address;
const contractAbi = ForemanContractJSON.abi;

ForemanContract = new web3.eth.Contract(contractAbi, contractAddress);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/jquery',express.static(path.join(__dirname+'/node_modules/jquery/dist/')));
app.use(express.static(path.join(__dirname, '/public')));

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false
}));
var ssn;

app.use(passport.initialize());
app.use(passport.session());

app.use('/', homeRouter);
app.use('/foreman', foremanRouter);
app.use('/subscriber', subscriberRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
