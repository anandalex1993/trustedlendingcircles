var express = require('express');
var router = express.Router();
var subscriberFundCollection = require('../models/subscriberMyFundModel');

// Require controller modules.
var foreman_controller = require('../controllers/foremanController');
var trusted_lending_circle_controller = require('../controllers/trustedLendingCircleController');

///   SUBSCRIBER ROUTES   ///

// GET subscriber home page.
router.get('/', foreman_controller.fund_log_subscriber);

//for checking joining number
router.get('/view/:trustedLendingCircleAddress/:fundName/:fundInstallment/:maxTicket', trusted_lending_circle_controller.subscriber_log);

// store subscriber fund address in DB
router.get('/subscriberFundDbStore', (req, res) => {
    ssn = req.session;
    try {
        const subscriberFund = new subscriberFundCollection({
            subscriber_id: ssn.user_id,
            fundName: req.query.fundName,
            fundAddress: req.query.fundAddress
        });

        subscriberFund.save()
            .then((post) => {
                res.status(201).json({
                    message: 'success'
                });
            })
            .catch((err) => {
                res.status(500).json({
                    message: 'error'
                })
            })
    } catch (e) {
        throw (e);
    }
});

//GET my fund page
router.get('/myFund', (req, res) => {
    if (req.isAuthenticated()) {
        ssn = req.session;
        var fundArray = [];
        subscriberFundCollection.find()
            .where('subscriber_id').equals(ssn.user_id)
            .select('fundName fundAddress')
            .then((result) => {
                res.render('subscriber/myFund/myFund', { result: result });
            });
    } else {
        res.redirect('/login');
    }
});

//GET Subscription Log Page
router.get('/subscriberLog/:contractAddress', (req, res) => {
    if (req.isAuthenticated()) {
        res.render('subscriber/myFund/subscriberLog', { contractAddress: req.params.contractAddress });
    } else {
        res.redirect('/login');
    }
});

//GET Subscription Log event
router.get('/subscriberLogEvent', trusted_lending_circle_controller.subscriber_log_for_specific_subscriber);

//Get Pay Subscription Page
router.get('/paySubscription/:contractAddress', (req, res) => {
    if (req.isAuthenticated()) {
        res.render('subscriber/myFund/paySubscription', { contractAddress: req.params.contractAddress });
    } else {
        res.redirect('/login');
    }
});

//GET Pay Subscription Amount to be Paid
router.get('/paySubscriptionAmount', trusted_lending_circle_controller.subscriber_log_for_specific_subscriber);

//GET auction log page for subscriber
router.get('/auctionLog/:contractAddress', (req,res) => {
    if(req.isAuthenticated()) {
        res.render('subscriber/auction/auctionLog', {contractAddress: req.params.contractAddress});
    } else {
        res.redirect('/login');
    }
});

// GET Auction Log events
router.get('/auctionLogEvent', trusted_lending_circle_controller.auction_log_event);

//GET Auction Event Log Events
router.get('/auctionEventLogEvent', trusted_lending_circle_controller.auction_event_log_event);

//GET Balance Log Page
router.get('/balanceLog/:contractAddress', (req,res) => {
    if(req.isAuthenticated()) {
        res.render('subscriber/myFund/balanceLog', {contractAddress: req.params.contractAddress});
    } else {
        res.redirect('/login');
    }
});

//GET Balance Log events
router.get('/balanceLogEvent', trusted_lending_circle_controller.balance_log_for_specific_subscriber);

module.exports = router;
