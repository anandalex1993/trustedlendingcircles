var express = require('express');
const { default: Web3 } = require('web3');
var router = express.Router();

// Require controller modules.
var foreman_controller = require('../controllers/foremanController');
var trusted_lending_circle_controller = require('../controllers/trustedLendingCircleController');

/* GET Foreman Home Page */
router.get('/', foreman_controller.fund_log_foreman_all);

/* GET Form to create new Circle */
router.get('/newCircleForm', function (req, res, next) {
  if (req.isAuthenticated()) {
    res.render('foreman/newCircleForm', { title: 'New Trusted Lending Circle' });
  } else {
    res.redirect('/login');
  }
});

/* GET foreman my circle page */
router.get('/mycircles', (req, res) => {
  if (req.isAuthenticated()) {
    res.render('foreman/mycircles', { events: eventArr });
  } else {
    res.redirect('/login');
  }
});

/* GET foreman created circles */
router.get('/myfundcircle', foreman_controller.fund_log_foreman_my);

/* GET subsriber log page */
router.get('/subscriberLog/:contractAddress',(req,res) => {
  if (req.isAuthenticated()) {
    res.render('foreman/subscriberLog',{ title: 'Home', contractAddress: req.params.contractAddress});
  } else {
    res.redirect('/login');
  }
})

/* GET subscriber log events */
router.get('/subscriberLogEvent', trusted_lending_circle_controller.subscriber_log_foreman);

/* GET balance log page */
router.get('/balanceLog/:contractAddress', (req,res) => {
  if (req.isAuthenticated()) {
    res.render('foreman/balanceLog',{ title: 'Home', contractAddress: req.params.contractAddress});
  } else {
    res.redirect('/login');
  }
});

/* GET balance log events */
router.get('/balanceLogEvent', trusted_lending_circle_controller.balance_log_foreman);

/* GET Installmnet Log Page */
router.get('/installmentLog/:contractAddress', (req,res) => {
  if (req.isAuthenticated()) {
    res.render('foreman/installmentLog',{ title: 'Home', contractAddress: req.params.contractAddress});
  } else {
    res.redirect('/login');
  }
});

/* Upadate Installment log */
router.get('/installmentLogEvent', trusted_lending_circle_controller.installment_log_foreman);

/* Update Installment Index by Foreman */
router.get('/updateInstallmentIndex', trusted_lending_circle_controller.update_installment_index);

// GET Auction Log Page
router.get('/auctionLog/:contractAddress', (req,res) => {
  if(req.isAuthenticated()) {
    res.render('foreman/auction/auctionLog', {title: 'Auction Log', contractAddress: req.params.contractAddress});
  } else {
    res.redirect('/login');
  }
});

// GET Auction Log events
router.get('/auctionLogEvent', trusted_lending_circle_controller.auction_log_event);

//GET Auction Event Log Events
router.get('/auctionEventLogEvent', trusted_lending_circle_controller.auction_event_log_event);

// GET Verfication page
router.get('/verificationLog/:contractAddress/:installments/:installmentAmount', (req,res) => {
  if (req.isAuthenticated()) {
    const contractAddress = req.params.contractAddress;
    const contractAbi = TrustedLendingCircleContractJSON.abi;

    TrustedLendingCircleContract = new web3.eth.Contract(contractAbi, contractAddress);
    TrustedLendingCircleContract.getPastEvents(
      'installmentLog',
      { fromBlock: 0, toBlock: 'latest' },
      (err, events) => {
          installmentIndex = events.length;
          TrustedLendingCircleContract.getPastEvents(
            'winnerLog',
            {
              filter: {installmentIndex: installmentIndex},
              fromBlock: 0, toBlock: 'latest' },
            (err, evnt) => {
              if (evnt.length != 0) {
                res.render('foreman/verify/verifyLog', {
                  contractAddress: req.params.contractAddress,
                  installmentIndex: installmentIndex,
                  installments: req.params.installments,
                  installmentAmount: req.params.installmentAmount,
                  highestBid: evnt[0].returnValues[2]
                });
              } else {
                res.render('foreman/verify/verifyLog', {
                  contractAddress: req.params.contractAddress,
                  installmentIndex: installmentIndex,
                  installments: req.params.installments,
                  installmentAmount: req.params.installmentAmount,
                  highestBid: 0
                });
              }
            });
    });
  } else {
    res.redirect('/login');
  }
});

router.get('/verificationLogEvent', trusted_lending_circle_controller.verify_log_events);

// router.get('/verifySubscriber/:contractAddress/:installmentIndex/:installmentAmount/:ticketNumber/:subscriberAddress/:pay/:fine/:deposit/:balance/:winner/:subscriber/:due', (req,res) => {
//   if (req.isAuthenticated()) {
//     console.log('here wth');
//    res.render('foreman/verify/verifySubscriber', {
//       'contractAddress': req.body.contractAddress,
//       'installmentIndex': req.body.installmentIndex,
//       'installmentAmount': req.body.installmentAmount,
//       'installments': req.body.installments,
//       'ticketNumber': req.body.ticketNumber,
//       'subscriberAddress': req.body.subscriberAddress,
//       'pay': req.body.pay,
//       'fine': req.body.fine,
//       'deposit': req.body.deposit,
//       'balance': req.body.balance,
//       'winner': req.body.winner,
//       'subscriber': req.body.subscriber,
//       'due': req.body.due
//     });
//   } else {
//     res.redirect('/login');
//   }
// });

// router.post('/verifySubscriber', (req,res) => {
//   console.log(req.body);
//   res.render('foreman/verify/verifySubscriber');
// });

module.exports = router;
