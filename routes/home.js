var express = require('express');
var dbConnection = require('../models/db');
var userCollection = require('../models/userModel');
const bcrypt = require('bcrypt');
const passport = require('passport');
const saltRounds = 10;
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('home/home', { title: 'Trusted Lending Circles' });
});

/* GET signUp page. */
router.get('/signup', (req, res) => {
  res.render('home/signup');
});

/* GET signUp. */
router.post('/signup', (req, res) => {
  userCollection.register(new userCollection({
    walletAddress: req.body.walletAddress,
    password: req.body.password,
    role: req.body.role
  }), req.body.password, (err, user) => {
    if (err) {
      console.log(err);
    } else {
      passport.authenticate("local")(req,res, () => {
        if (req.body.role == 1) {
          res.redirect('/foreman');
        } else {
          res.redirect('/login');
        }
      });
    }
  });
});

/* GET Login page. */
router.get('/logIn', (req, res) => {
  res.render('home/login');
});

/* POST Login. */
router.post('/logIn', (req, res) => {
  const user = new userCollection({
    walletAddress: req.body.walletAddress,
    passport: req.body.password
  });
  ssn = req.session;
  req.login(user, (err) => {
    if(err) {
      console.log(err);
    } else {
      passport.authenticate("local")(req, res, function() {
        userCollection.findOne({walletAddress: req.body.walletAddress})
        .then((foundUser) => {
          if (foundUser.role == 1) {
            res.redirect('/foreman');
          } else {
            ssn.user_id = foundUser._id;
            res.redirect('/subscriber');
          }
        })
      })
    }
  });
});

router.get('/logout', (req,res) => {
  req.session.destroy((err) => {
    if (err) {
      console.log(err);
    } else {
      req.logout();
      res.redirect('/login');
    }
  });

});

module.exports = router;
