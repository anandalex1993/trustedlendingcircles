// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

import './Lib.sol';
import './SafeMath.sol';

contract TrustedLendingCircle{
    using SafeMath for *;
    using Lib for *;

    // fund variables
    uint public securityDeposit; // security deposit from foreman
    uint fundInstallment; // subscription amount for each installment
    uint maxTickets; // maximum number of subscribers allowed
    address foreman; // conductor of the fund
    uint foremanCommissionPerInstallment;
    uint foremanCommission; // commission of 5% for foreman for conducting fundClosed
    uint installmentIndex; // installment number
    bool fundCanceled; // flag to know fund got canceled
    bool fundClosed; // flag to know fund open or closed
    bool givePrize; // flag to give away the prize
    uint withDrawIndex; //flag to allow security deposit withdrawal

    //subscriber variables
    uint ticketIndex; // ticket number for subscription

    //auction variables
    uint bidStart; // bid starting time
    uint bidEnd; // bid end time
    uint highestBid; // highest bid

    //subscriber mappings
    mapping(address => uint) ticketNumber; // subscriber address mapped to ticket index
    mapping(uint => Lib.Subscriber) subscribers; // ticket index mapped to subscriber details

    //auction mappings
    mapping(uint => bool) auction; //auction conducted check
    mapping(uint => uint[]) auctionRegister; // to collect different auction details
    mapping(uint => address) winners; // to collect winners in each installment
    mapping(uint => bool) punish; // punish defaulters of each installment

    //Events
    event subscriberLog(
            uint indexed installmentIndex,
            uint ticketNumber,
            address indexed subscriberAddress,
            uint paid,
            uint fine,
            uint deposit,
            uint balance,
            bool isWinner,
            bool isSubscriber,
            Lib.Punishment indexed due,
            Lib.SubscriberStatus status
        );

    event balanceLog(
            uint indexed time,
            address indexed memberAddress,
            uint credit,
            uint debit,
            uint balanceOfContract
        );

    event installmentLog(
            uint indexed installmentIndex,
            uint toPay,
            bool fundClosed,
            bool fundCanceled
        );

    event auctionLog(
            uint indexed installmentIndex,
            uint bidStart,
            uint bidEnd
        );

    event auctionEventLog(
            uint indexed installmentIndex,
            uint timeStamp,
            uint indexed highestBid,
            uint[] subscriberAddress
        );

    event winnerLog(
            uint indexed installmentIndex,
            uint timeStamp,
            uint indexed highestBid,
            address winnerAddress
        );

    constructor(
            address _foreman,
            uint _securityDeposit,
            uint _fundInstallment,
            uint _maxTickets,
            uint _foremanCommissionPerInstallment
        )
    payable
    {
        foreman = _foreman;
        securityDeposit= _securityDeposit;
        fundInstallment= _fundInstallment;
        maxTickets= _maxTickets;
        foremanCommissionPerInstallment = _foremanCommissionPerInstallment;
    }

    //@dev Option For Subscribers to join fund
    function joinFund()
    public
    payable
    {
        // conditions
        require(msg.sender != foreman, "!F"); // foreman not allowed to pariticipate
        require(!subscribers[ticketNumber[msg.sender]].isSubscriber, "S"); // subscirber already exsist
        require(msg.value == fundInstallment, "!Am"); // insufficient installment amount
        require(ticketIndex < maxTickets, "!S"); // Subscriber limit exceeds

        // Effects
        ticketIndex++;
        securityDeposit = securityDeposit.add(msg.value); // payment added to the security deposit
        ticketNumber[msg.sender] = ticketIndex;
        subscribers[ticketIndex] = Lib.Subscriber(
                ticketIndex,
                installmentIndex,
                msg.sender,
                fundInstallment,
                0,
                fundInstallment,
                0,
                false,
                true,
                Lib.Punishment.NoDues,
                Lib.SubscriberStatus.Active,
                "",
                false
        );

        //Events
        emit subscriberLog(
            installmentIndex,
            ticketIndex,
            msg.sender,
            fundInstallment,
            0,
            fundInstallment,
            0,
            false,
            true,
            Lib.Punishment.NoDues,
            Lib.SubscriberStatus.Active
        );

        emit balanceLog(
            block.timestamp,
            msg.sender,
            msg.value,
            0,
            securityDeposit
        );
    }

    //@dev Option For Foreman to Update Installment index
    function updateInstallmentIndex()
    public
    {
        //conditions
        require(msg.sender == foreman, "!F"); // only foreman allowed
        require(ticketIndex == maxTickets, "!S"); // not enough subscribers to start installment

        // Effect
        if(installmentIndex >= 1) {
            require(winners[installmentIndex] != address(0), "!W"); //conduct auction and select winner
            require(punish[installmentIndex], "!P"); //subscirbers with due punished
        }

        installmentIndex++;
        if (installmentIndex == maxTickets) {
            fundClosed = true;
        }

        emit installmentLog(
                installmentIndex,
                0,
                fundClosed,
                fundCanceled
            );
    }

    //@dev Option for Subscribers to Pay subscription
    function paySubscription()
    public
    payable
    {
        //condition
        require(installmentIndex > 0, "!A"); // can only pay subscription for second installment
        require(!fundClosed && !fundCanceled, "FuCl");
        require(subscribers[ticketNumber[msg.sender]].isSubscriber, "!S"); //Only subscribers can pay installments
        require(subscribers[ticketNumber[msg.sender]].installmentNumber < installmentIndex, "Pa");
        require(msg.value == subscribers[ticketNumber[msg.sender]].toPay.add(subscribers[ticketNumber[msg.sender]].fine));

        //Effect
        securityDeposit = securityDeposit.add(msg.value);
        subscribers[ticketNumber[msg.sender]].deposit = subscribers[ticketNumber[msg.sender]].deposit.add(subscribers[ticketNumber[msg.sender]].toPay);
        subscribers[ticketNumber[msg.sender]].installmentNumber = installmentIndex;

        //Events
        emit subscriberLog(
            installmentIndex,
            ticketNumber[msg.sender],
            msg.sender,
            subscribers[ticketNumber[msg.sender]].toPay,
            subscribers[ticketNumber[msg.sender]].fine,
            subscribers[ticketNumber[msg.sender]].deposit,
            subscribers[ticketNumber[msg.sender]].balance,
            subscribers[ticketNumber[msg.sender]].isWinner,
            subscribers[ticketNumber[msg.sender]].isSubscriber,
            subscribers[ticketNumber[msg.sender]].due,
            subscribers[ticketNumber[msg.sender]].status
        );

        emit balanceLog(
            block.timestamp,
            msg.sender,
            msg.value,
            0,
            securityDeposit
        );

    }

    //@dev Option for Foreman to conduct auction
    function conductAuction(uint _bidTime)
    public
    {
        //Conditions
        require(installmentIndex > 0, "!A"); // no auction for first installmentIndex
        require(msg.sender == foreman, "!F"); // only foreman can conduct auction
        require(auction[installmentIndex] == false, "A"); //conduct auction only when no auction conducted for this installmentIndex

        //Effect
        auction[installmentIndex] = true;
        bidStart = block.timestamp;
        bidEnd = bidStart + _bidTime * 1 minutes;
        delete auctionRegister[highestBid];
        delete highestBid;

        //Events
        emit auctionLog(
                installmentIndex,
                bidStart,
                bidEnd
            );
    }

    //@dev Option for Subscribers to bid in auction
    function bid(uint _bidAmount)
    public
    {
        // Conditions
        require(subscribers[ticketNumber[msg.sender]].isSubscriber, "!S1"); // only subscribers can pariticipate
        require(!subscribers[ticketNumber[msg.sender]].isWinner, "W"); // winners cant pariticipate again in auction
        require(subscribers[ticketNumber[msg.sender]].installmentNumber == installmentIndex, "!S2"); // subscriber with due cant pariticipate
//------------commet bid and bid end for the test work ---------------------------------------------//
        require(bidStart < block.timestamp, "S"); //can bid only after start time
        require(bidEnd > block.timestamp, "E"); //no bidding after end time
        
        require(auctionRegister[highestBid].ticketIndexOf(ticketNumber[msg.sender]) != uint(-1), "!S3"); //already bid for same amount
        //Effects
        if(highestBid < _bidAmount) {
            highestBid = _bidAmount;
        }

        auctionRegister[highestBid].push(ticketNumber[msg.sender]);

        //Events
        emit auctionEventLog(
                installmentIndex,
                block.timestamp,
                highestBid,
                auctionRegister[highestBid]
            );
    }

    //@dev Option for Foreman to Pick winner
    function pickWinner()
    public
    {
        //Conditions
        require(msg.sender == foreman, "!S"); // only foreman allowed to pick winner
    //--------------------Commet bid running to test to work ------------------------------//
        require(bidEnd < block.timestamp, "A"); // bid running
        require(winners[installmentIndex] == address(0), "W"); // winner already selected for this installment

        //Effects
        if (auctionRegister[highestBid].length == 0) {
            winners[installmentIndex] = subscribers[Lib.findInMapping(subscribers, maxTickets)].subscriberAddress;
            subscribers[ticketNumber[winners[installmentIndex]]].isWinner = true;
            //highestBid = ((fundInstallment.mul(maxTickets)).div(100)).mul(70);
            highestBid = fundInstallment*(installmentIndex+1);

        } else if (auctionRegister[highestBid].length == 1) {
            winners[installmentIndex] = subscribers[auctionRegister[highestBid][0]].subscriberAddress;
            subscribers[ticketNumber[winners[installmentIndex]]].isWinner = true;

        } else {
            // lottery
            uint randomNumb = maxTickets.random(auctionRegister[highestBid].length);

            while (auctionRegister[highestBid].ticketIndexOf(randomNumb) == uint(-1)) {
                randomNumb = maxTickets.random(auctionRegister[highestBid].length);
            }

            winners[installmentIndex] = subscribers[auctionRegister[highestBid][randomNumb]].subscriberAddress;
            subscribers[ticketNumber[winners[installmentIndex]]].isWinner = true;

        }

        givePrize = true;

        //Events
        emit subscriberLog(
            installmentIndex,
            ticketNumber[winners[installmentIndex]],
            winners[installmentIndex],
            subscribers[ticketNumber[winners[installmentIndex]]].toPay,
            subscribers[ticketNumber[winners[installmentIndex]]].fine,
            subscribers[ticketNumber[winners[installmentIndex]]].deposit,
            subscribers[ticketNumber[winners[installmentIndex]]].balance,
            subscribers[ticketNumber[winners[installmentIndex]]].isWinner,
            subscribers[ticketNumber[winners[installmentIndex]]].isSubscriber,
            subscribers[ticketNumber[winners[installmentIndex]]].due,
            subscribers[ticketNumber[winners[installmentIndex]]].status
        );

        emit winnerLog(
                installmentIndex,
                block.timestamp,
                highestBid,
                winners[installmentIndex]
            );
    }

    //@dev Option for winner to provide attachment certificate

    //@dev Option for foreman to verify attachment certificate

    //@dev Option for winner to Recieve Prize
    function getPrize()
    public
    {
        //Condidions
        require(subscribers[ticketNumber[msg.sender]].isWinner, "!W"); //only winner can access this function
        require(winners[installmentIndex] != address(0), "!Wi"); //no winner available for this round of installment
        require(givePrize, "!P"); // prize already collected

        //Effects
        givePrize = false;
        securityDeposit = securityDeposit.sub(highestBid);

        subscribers[ticketNumber[msg.sender]].balance = highestBid;

        if ( subscribers[ticketNumber[winners[installmentIndex]]].attached == true) {
            // winner can have full prize money if subscriber provide security for the prize amount.
            uint withdrawPrize = subscribers[ticketNumber[msg.sender]].balance;
            delete subscribers[ticketNumber[msg.sender]].balance;
            msg.sender.transfer(withdrawPrize);
            givePrize = false;

            //Events
            emit balanceLog(
                    block.timestamp,
                    msg.sender,
                    0,
                    withdrawPrize,
                    securityDeposit
                );

        } else {
            // winner only gets the deposited money back if he doesn't provide security
            // uint withdrawDeposit = subscribers[ticketNumber[msg.sender]].deposit;
            // subscribers[ticketNumber[msg.sender]].balance = subscribers[ticketNumber[msg.sender]].balance.sub(withdrawDeposit);
            // msg.sender.transfer(subscribers[ticketNumber[msg.sender]].deposit);
            // delete subscribers[ticketNumber[msg.sender]].deposit;
            // givePrize = false;

            uint withdrawDeposit = subscribers[ticketNumber[msg.sender]].deposit;
            delete subscribers[ticketNumber[msg.sender]].deposit;

            if (withdrawDeposit > highestBid) {
                // less than commission reduce
                withdrawDeposit = withdrawDeposit.sub(foremanCommissionPerInstallment);
                securityDeposit = securityDeposit.add(foremanCommissionPerInstallment);
                delete subscribers[ticketNumber[msg.sender]].balance;
            } else {
                subscribers[ticketNumber[msg.sender]].balance = highestBid.sub(withdrawDeposit);
            }

            msg.sender.transfer(withdrawDeposit);
            givePrize = false;

            emit balanceLog(
                    block.timestamp,
                    msg.sender,
                    0,
                    withdrawDeposit,
                    securityDeposit
                );
        }

    }

    //@dev Foreman punishes the defaulters using the events
    function punishSubscriber(
        address _subscriberAddress,
        uint _punishment,
        uint _fineDue,
        uint _fineWinner,
        uint _fineDue2,
        uint _fineWinner2,
        uint _interestDividend
        )
    public
    {
        //Condidions
        require(msg.sender == foreman, "!F"); // only foreman authorized
        require(winners[installmentIndex] != address(0), "!W"); // only after picking winner to this installment

        //Effects
        if (_punishment == 0) {
            // no dues
            subscribers[ticketNumber[_subscriberAddress]].toPay = fundInstallment.sub(_interestDividend);

        } else if (_punishment == 1) {
            //due
            subscribers[ticketNumber[_subscriberAddress]].due =  Lib.Punishment.Due;
            subscribers[ticketNumber[_subscriberAddress]].toPay = fundInstallment;
            subscribers[ticketNumber[_subscriberAddress]].fine = _fineDue;

        } else if (_punishment == 2) {
            //winnerDue
            subscribers[ticketNumber[_subscriberAddress]].due =  Lib.Punishment.WinnerDue;
            subscribers[ticketNumber[_subscriberAddress]].toPay = fundInstallment;
            subscribers[ticketNumber[_subscriberAddress]].fine = _fineWinner;

        } else if (_punishment == 3) {
            //SellSubscription
            subscribers[ticketNumber[_subscriberAddress]].due =  Lib.Punishment.SellSubscription;
            subscribers[ticketNumber[_subscriberAddress]].toPay = fundInstallment;
            subscribers[ticketNumber[_subscriberAddress]].fine = _fineDue2;

            // change fund ownership to foreman
            uint ticket = ticketNumber[_subscriberAddress];
            delete ticketNumber[_subscriberAddress];
            ticketNumber[foreman] = ticket;

        } else if (_punishment == 4) {
            //Attachment
            subscribers[ticketNumber[_subscriberAddress]].due =  Lib.Punishment.Attachment;
            subscribers[ticketNumber[_subscriberAddress]].toPay = fundInstallment;
            subscribers[ticketNumber[_subscriberAddress]].fine = _fineWinner2;

            // change fund ownership to foreman
            uint ticket = ticketNumber[_subscriberAddress];
            delete ticketNumber[_subscriberAddress];
            ticketNumber[foreman] = ticket;
        }

    }

    //@dev Foreman can assert subscribers being punished
    function verifySubsciptions()
    public
    {
        require(msg.sender == foreman, "!F"); // only foreman authorized
        punish[installmentIndex] = true;
        foremanCommission = foremanCommission.add(foremanCommissionPerInstallment);
        securityDeposit = securityDeposit.sub(foremanCommissionPerInstallment);
    }

    //@dev Option for Foreman to cancel fund
    function cancelFund()
    public
    {
        //Condidions
        require(msg.sender == foreman, "!F"); //only forman authorized
        require(installmentIndex == 0, "!I"); // can cancel fund only during initial stage
        require(ticketIndex < maxTickets.sub(1), "!S"); // participants number is less

        //Effects
        fundCanceled = true;

        //Event
         emit installmentLog(
            installmentIndex,
            0,
            fundClosed,
            fundCanceled
        );
    }

    //@dev Option for Subscribers to with draw Deposit and foreman to withDraw foreman Commission
    function withDraw()
    public
    {
        require(fundClosed || fundCanceled, "FuAc");

        if(msg.sender == foreman) {
            uint commission = foremanCommission;
            delete foremanCommission;
            withDrawIndex++;
            securityDeposit = securityDeposit.sub(commission);
            msg.sender.transfer(commission);

            emit balanceLog(
                block.timestamp,
                msg.sender,
                0,
                commission,
                securityDeposit
            );
        } else {
            if (fundCanceled) {
                uint giveDeposit = subscribers[ticketNumber[msg.sender]].deposit;
                delete subscribers[ticketNumber[msg.sender]].balance;
                delete subscribers[ticketNumber[msg.sender]].deposit;
                withDrawIndex++;
                securityDeposit = securityDeposit.sub(giveDeposit);
                msg.sender.transfer(giveDeposit);
            } else {
                uint withDrawAmount = subscribers[ticketNumber[msg.sender]].isWinner
                ? subscribers[ticketNumber[msg.sender]].balance
                : subscribers[ticketNumber[msg.sender]].deposit.sub(foremanCommissionPerInstallment);

                delete subscribers[ticketNumber[msg.sender]].balance;
                delete subscribers[ticketNumber[msg.sender]].deposit;
                withDrawIndex++;
                if (!subscribers[ticketNumber[msg.sender]].isWinner) {
                    foremanCommission = foremanCommission.add(foremanCommissionPerInstallment);
                    securityDeposit = securityDeposit.add(foremanCommissionPerInstallment);
                    securityDeposit = securityDeposit.sub(withDrawAmount);
                }

                msg.sender.transfer(withDrawAmount);

                emit balanceLog(
                        block.timestamp,
                        msg.sender,
                        0,
                        withDrawAmount,
                        securityDeposit
                    );
                }

        }


    }

    //@dev verify subscriber withdrawed their deposit
    function deleteContract()
    public
    view
    returns
    (bool success)
    {
        require(fundClosed || fundCanceled, "Fu"); // fund running
        require(withDrawIndex > maxTickets, "!S"); // subscriber remaining to withdraw cash

        return true;
    }

    //@dev Self destruct
    function run (address target, string calldata func, address to) public {
        target.delegatecall(abi.encodeWithSignature(func, to));

    }
}
