// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

library Lib{
    /// store fund details
    struct Fund {
        uint fundNumber;
        address fundAddress;
        address foreman;
        uint installmentAmount;
        uint noOfInstallments;
        FundStatus status;
    }

    /// store foreman details
    struct ForemanRegister {
        uint fundIndex;
        address fundAddress;
        uint securityDeposit;
        FundStatus status;
    }

    /// store subscribers details
    struct Subscriber {
        uint ticketNumber; // roll number
        uint installmentNumber;
        address subscriberAddress; // subscriber address
        uint toPay; // subscription amount to pay
        uint fine; // fine for the due
        uint deposit; // total amount paid
        uint balance; // amount that can be withdrawed
        bool isWinner; // winner in lottery check
        bool isSubscriber; // subscriber to the fund check
        Punishment due;
        SubscriberStatus status;
        bytes32 attachmentName; // security to withdraw full prize money
        bool attached; // attachment verified by foreman
    }

    enum FundStatus {Inactive, Active} // fund status
    enum SubscriberStatus {Inactive, Active} // subscriber status
    enum Punishment {NoDues, Due, WinnerDue, SellSubscription, Attachment } // punishment for defaulting the payment

   //@dev search a ticket number in a array
   function ticketIndexOf(uint[] memory self, uint ticketNumber) internal pure returns (uint8) {
       for(uint8 i = 0; i < self.length; i++) if (self[i] == ticketNumber) return i;
       return uint8(-1);
   }

   //@dev find lowest ticket who is not a Winner
   function findInMapping(mapping(uint => Subscriber) storage self, uint maxTickets) internal view returns (uint) {
       for(uint i = 1; i <= maxTickets; i++) if (self[i].isWinner == false) return i;
       return uint(-1);
   }

   //@dev get random number
   function random(uint self, uint auctionArrayLength) internal view returns (uint) {
      return uint256(keccak256(abi.encodePacked(block.timestamp, block.difficulty, auctionArrayLength)))%self;
    }
}
