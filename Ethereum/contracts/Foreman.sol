// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

import './Lib.sol';
import './SafeMath.sol';
import './TrustedLendingCircle.sol';

contract Foreman {
    TrustedLendingCircle fundContract;

    using Lib for *;
    using SafeMath for *;

    // variables
    uint fundIndex; // fund index


    //mapping
    mapping(uint => Lib.Fund) trustedLendingCicles; // fund colleciton
    mapping(address => Lib.ForemanRegister) foremen; // forman register

    event fundLog(
        uint indexed fundIndex,
        string fundName,
        address fundAddress,
        address indexed foreman,
        uint installmentAmount,
        uint noOfInstallments,
        Lib.FundStatus indexed status
    );

    /// @dev store the fund details in trustedLendingCircles
    function createFund(
        string memory _fundName,
        uint _fundInstallment,
        uint _maxTicketNumber,
        uint _foremanCommission
    )
        public
        payable
    {
        //Condition
        require(foremen[msg.sender].fundAddress == address(0), "Fo"); // foreman only start one fund
        require(msg.value == _fundInstallment.mul(_maxTicketNumber), "!SD"); // security deposit amount

        //Effect
        fundIndex = fundIndex.add(1);
        address _newFundAddress = address(
            new TrustedLendingCircle(
                msg.sender,
                msg.value,
               _fundInstallment,
               _maxTicketNumber,
               _foremanCommission
            )
        );

        // store fund details to fund registry
       trustedLendingCicles[fundIndex] = Lib.Fund(
            fundIndex,
            _newFundAddress,
            msg.sender,
            _fundInstallment,
            _maxTicketNumber,
            Lib.FundStatus.Active
        );

        // store fund details to foremen registry
        foremen[msg.sender] = Lib.ForemanRegister(
            fundIndex,
            _newFundAddress,
            msg.value,
            Lib.FundStatus.Active
        );

        //Event
        emit fundLog(
            fundIndex,
            _fundName,
            _newFundAddress,
            msg.sender,
            _fundInstallment,
            _maxTicketNumber,
            Lib.FundStatus.Active
        );
    }

    //@dev fund address for each foreman
    function getDeployedFund()
    public
    view
    returns(address)
    {
        require(foremen[msg.sender].fundAddress != address(0), "!Fo");
        return foremen[msg.sender].fundAddress;
    }

     //@dev update FundStatus and self destruct fund if needed
    function updateFundStatus(string memory _fundName)
    public
    {
        //Conditions
        require(foremen[msg.sender].fundAddress != address(0), "!Fo");

        //Effects
        fundContract = TrustedLendingCircle(foremen[msg.sender].fundAddress);
        bool success = fundContract.deleteContract();

        if (success == true) {
            kill();

            //Events
            emit fundLog(
                foremen[msg.sender].fundIndex,
                _fundName,
                foremen[msg.sender].fundAddress,
                msg.sender,
                trustedLendingCicles[foremen[msg.sender].fundIndex].installmentAmount,
                trustedLendingCicles[foremen[msg.sender].fundIndex].noOfInstallments,
                Lib.FundStatus.Inactive
            );
        }
    }

    //@dev destruct fund
    function kill () private {
        address payable to = msg.sender;
        selfdestruct(to);
    }
}
