const Foreman = artifacts.require("Foreman");
const TrustedLendingCircle = artifacts.require("TrustedLendingCircle");
const utils = require("./helpers/utils");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("Foreman", function (accounts) {
  let [foreman, subscriber1, subscriber2, subscriber3, subscriber4, subscriber5] = accounts;
  let foremanInstance;
  let trustedLendingCirclesAddress;
  let trustedLendingCirclesInstance;
  let _securityDeposit = 30000000000000000000;
  let _fundInstallment = "10000000000000000000";
  let _maxTicketNumber = "3";
  let winner1;

  // - Only One Bidder - No Defaulters
  it("Foreman Deployed", function () {
    Foreman.deployed();
    return assert.isTrue(true);
  });

  // Foreman create the fund
  it("Trusted Lending Circles Created", async function () {
    foremanInstance = await Foreman.deployed();
    await foremanInstance.createFund("Fund", _fundInstallment, _maxTicketNumber, "1500000000000000000",{
      from: foreman,
      value: _securityDeposit
    });
    trustedLendingCirclesAddress = await foremanInstance.getDeployedFund.call({ from: foreman });
    trustedLendingCirclesInstance = await TrustedLendingCircle.at(trustedLendingCirclesAddress);
    return assert.isTrue(true);
  });

  // subscribers join to the fund
  it("join fund", async () => {
    await trustedLendingCirclesInstance.joinFund({ from: subscriber1, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
    await utils.shouldThrow(trustedLendingCirclesInstance.joinFund({ from: subscriber1, value: 10000000000000000000 }));
  });

  it("join more subscibers", async () => {
    await trustedLendingCirclesInstance.joinFund({ from: subscriber2, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
    await trustedLendingCirclesInstance.joinFund({ from: subscriber3, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
    await utils.shouldThrow(trustedLendingCirclesInstance.joinFund({ from: subscriber4, value: 10000000000000000000 }));
  });

  it("update installment index", async () => {
    await trustedLendingCirclesInstance.updateInstallmentIndex({ from: foreman })
      .then((response) => {
        //console.log(response.logs);
      });
    return assert.isTrue(true);
  });

  // INSTALLMENT INDEX 1
  it("pay subscription", async () => {
    await trustedLendingCirclesInstance.paySubscription({ from: subscriber1, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
      await trustedLendingCirclesInstance.paySubscription({ from: subscriber2, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
      await trustedLendingCirclesInstance.paySubscription({ from: subscriber3, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
      await utils.shouldThrow(trustedLendingCirclesInstance.paySubscription({ from: subscriber4, value: 10000000000000000000 }));
  });

  // foreman conducts auction for the first installment
  it("conduct auction", async () => {
    await trustedLendingCirclesInstance.conductAuction("1", { from: foreman })
      .then((response) => {
        //console.log(response.logs);
      });
    return assert.isTrue(true);
  });


  // one bidder for the first installment
  it("bid", async function () {
    await trustedLendingCirclesInstance.bid("28500000000000000000", { from: subscriber3 })
      .then((response) => {
        //console.log(response.logs);
      });
      await utils.shouldThrow(trustedLendingCirclesInstance.bid("28500000000000000000", {from: subscriber4 }));
  });


  // foreman picks subscriber3 as winner
  it("pickWinner", async function () {
    await trustedLendingCirclesInstance.pickWinner({ from: foreman })
      .then((response) => {
        //console.log(response.logs);
        // return assert.equal(response.logs[0].event, "winnerLog", 'winnerLog event should fire.');
      });
      await utils.shouldThrow(trustedLendingCirclesInstance.pickWinner({from: subscriber4 }));
  });


  //subscriber 3 collect the prize money
  it('get prize', async function () {
    await trustedLendingCirclesInstance.getPrize({from: subscriber3})
    .then((response) => {
      //console.log(response.logs);
    })
    await utils.shouldThrow(trustedLendingCirclesInstance.getPrize({from: subscriber4 }));
  });

  //foreman verify every subscribers
  it('verify subscriber', async function() {
    await trustedLendingCirclesInstance.punishSubscriber(
      subscriber1, "0", "0", "0", "0", "0", "0", {from: foreman}
    )
    .then((response) => {
      //console.log(response.logs);
    });
    await trustedLendingCirclesInstance.punishSubscriber(
      subscriber2, "0", "0", "0", "0", "0", "0", {from: foreman}
    )
    .then((response) => {
      //console.log(response.logs);
    });
    await trustedLendingCirclesInstance.punishSubscriber(
      subscriber3, "0", "0", "0", "0", "0", "0", {from: foreman}
    )
    .then((response) => {
      //console.log(response.logs);
    });
  });

  //foreman collects verification commiossion
  it('commission', async function () {
    await trustedLendingCirclesInstance.verifySubsciptions({from: foreman})
    .then((response) => {
      //console.log(response.logs)
    })
  });

  // INSTALLMENT INDEX 2
  it("update installment index", async () => {
    await trustedLendingCirclesInstance.updateInstallmentIndex({ from: foreman })
      .then((response) => {
        //console.log(response.logs);
      });
    return assert.isTrue(true);
  });

  it("pay subscription", async () => {
    await trustedLendingCirclesInstance.paySubscription({ from: subscriber1, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
      await trustedLendingCirclesInstance.paySubscription({ from: subscriber2, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
      await trustedLendingCirclesInstance.paySubscription({ from: subscriber3, value: 10000000000000000000 })
      .then((response) => {
        //console.log(response.logs);
      });
    return assert.isTrue(true);
  });

  it("conduct auction", async () => {
    await trustedLendingCirclesInstance.conductAuction("1", { from: foreman })
      .then((response) => {
        //console.log(response.logs);
      });
    return assert.isTrue(true);
  });

  it("bid subscriber2", async function () {
    await trustedLendingCirclesInstance.bid("28500000000000000000", { from: subscriber2 })
      .then((response) => {
        //console.log(response.logs);
      });
  });

  it("pickWinner", async function () {
    await trustedLendingCirclesInstance.pickWinner({ from: foreman })
      .then((response) => {
        //console.log(response.logs);
        // return assert.equal(response.logs[0].event, "winnerLog", 'winnerLog event should fire.');
      });
    return assert.isTrue(true);
  });

  it('get prize', async function () {
    await trustedLendingCirclesInstance.getPrize({from: subscriber2})
    .then((response) => {
      //console.log(response.logs);
    })
  });

  it('verify subscriber', function() {
    trustedLendingCirclesInstance.punishSubscriber(
      subscriber1, "0", "0", "0", "0", "0", "0", {from: foreman}
    )
    .then((response) => {
      //console.log(response.logs);
    });
    trustedLendingCirclesInstance.punishSubscriber(
      subscriber2, "0", "0", "0", "0", "0", "0", {from: foreman}
    )
    .then((response) => {
      //console.log(response.logs);
    });
    trustedLendingCirclesInstance.punishSubscriber(
      subscriber3, "0", "0", "0", "0", "0", "0", {from: foreman}
    )
    .then((response) => {
      //console.log(response.logs);
    });
  });

  it('commission', function () {
    trustedLendingCirclesInstance.verifySubsciptions({from: foreman})
    .then((response) => {
      //console.log(response.logs)
    })
  });

  it("update installment index", () => {
    trustedLendingCirclesInstance.updateInstallmentIndex({ from: foreman })
      .then((response) => {
        //console.log(response.logs);
      });
    return assert.isTrue(true);
  });


  // subscribers withdraws amount
  it('with draw balance', async () => {
    await trustedLendingCirclesInstance.withDraw({from: foreman})
    .then(response => {
      //console.log(response.logs);
    });
    await trustedLendingCirclesInstance.withDraw({from: subscriber1})
    .then(response => {
      //console.log(response.logs);
    });
    await trustedLendingCirclesInstance.withDraw({from: subscriber2})
    .then(response => {
      //console.log(response.logs);
    });
    await trustedLendingCirclesInstance.withDraw({from: subscriber3})
    .then(response => {
      //console.log(response.logs);
    });
  });

  //foreman gets his security deposit
  it('delete contract', async () => {
    foremanInstance = await Foreman.deployed();
    await foremanInstance.updateFundStatus("Fund",{
      from: foreman
    });
  });

  // After this test the foreman earns 4.5 ether as reward and subscribers save 28.5 ether
});
