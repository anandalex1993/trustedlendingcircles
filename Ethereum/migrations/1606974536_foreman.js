const Lib = artifacts.require("Lib");
const SafeMath = artifacts.require("SafeMath");
const Foreman = artifacts.require("Foreman");

module.exports = function (deployer) {
  deployer.deploy(Lib);
  deployer.deploy(SafeMath);
  deployer.deploy(Foreman);
  deployer.link(Lib, Foreman);
  deployer.link(SafeMath, Foreman);
};
