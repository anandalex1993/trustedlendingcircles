const mongoose = require('mongoose');

const subscriberMyFundSchema = mongoose.Schema({
    subscriber_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "usercollections",
        required: true
    },
    fundName: {
        type: String
    },
    fundAddress: {
        type: String
    }
  });

  var subscriberFundCollection = mongoose.model("subscriberFundcollections", subscriberMyFundSchema);

  module.exports = subscriberFundCollection;
