const mongoose = require('mongoose');

//DataBase Connection//
mongoose.connect( process.env.DB_API, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
}).then(() => {
  console.log("Connected to DataBase");
})
  .catch((error) => {
    console.log(error);
    console.log("Connection Failed!!!");
  });
