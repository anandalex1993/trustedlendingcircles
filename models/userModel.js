const mongoose = require('mongoose');
var encrypt = require('mongoose-encryption');
var passport = require("passport");
const uniqueValidator = require('mongoose-unique-validator');
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = mongoose.Schema({
    walletAddress: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: Number,
      required: true
    }
  });

  userSchema.plugin(uniqueValidator);
  userSchema.plugin(encrypt, { secret:  process.env.SECRET, encryptedFields: ['password'] });
  userSchema.plugin(passportLocalMongoose, {
    usernameField: 'walletAddress'
  });

  var userCollection = mongoose.model("usercollections", userSchema);

  passport.use(userCollection.createStrategy());
  passport.serializeUser(userCollection.serializeUser());
  passport.deserializeUser(userCollection.deserializeUser());

  module.exports = userCollection;
