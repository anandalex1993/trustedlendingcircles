function connectToMetMask() {
    ethereum.enable().then(result => {
        accounts = result[0];
    })
}
$(function () {
    // ethereum.enable();
    if (typeof web3 !== 'undefined') {
        web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        window.web3 = new Web3("http://localhost:8545");
    }

    const contractAddress = "0x30000e6Acc01448e776da6046E4AB3A0A0a90A7b";
    const contractAbi = [
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "internalType": "uint256",
                    "name": "fundIndex",
                    "type": "uint256"
                },
                {
                    "indexed": false,
                    "internalType": "string",
                    "name": "fundName",
                    "type": "string"
                },
                {
                    "indexed": false,
                    "internalType": "address",
                    "name": "fundAddress",
                    "type": "address"
                },
                {
                    "indexed": true,
                    "internalType": "address",
                    "name": "foreman",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "installmentAmount",
                    "type": "uint256"
                },
                {
                    "indexed": false,
                    "internalType": "uint256",
                    "name": "noOfInstallments",
                    "type": "uint256"
                },
                {
                    "indexed": true,
                    "internalType": "enum Lib.FundStatus",
                    "name": "status",
                    "type": "uint8"
                }
            ],
            "name": "fundLog",
            "type": "event"
        },
        {
            "inputs": [
                {
                    "internalType": "string",
                    "name": "_fundName",
                    "type": "string"
                },
                {
                    "internalType": "uint256",
                    "name": "_fundInstallment",
                    "type": "uint256"
                },
                {
                    "internalType": "uint256",
                    "name": "_maxTicketNumber",
                    "type": "uint256"
                }
            ],
            "name": "createFund",
            "outputs": [],
            "stateMutability": "payable",
            "type": "function",
            "payable": true
        },
        {
            "inputs": [],
            "name": "getDeployedFund",
            "outputs": [
                {
                    "internalType": "address",
                    "name": "",
                    "type": "address"
                }
            ],
            "stateMutability": "view",
            "type": "function",
            "constant": true
        },
        {
            "inputs": [
                {
                    "internalType": "string",
                    "name": "_fundName",
                    "type": "string"
                }
            ],
            "name": "updateFundStatus",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }
    ];

    myContract = new web3.eth.Contract(contractAbi, contractAddress);

    $('#create').submit(function (e) {
        e.preventDefault();
        fundName = $('#create input[name="fundName"]').val();
        fundInstallment = $('#create input[name="fundInstallment"]').val();
        maxTicketNumber = $('#create input[name="maxTicketNumber"]').val();

        web3.eth.getAccounts().then(function (accounts) {
            myContract.methods.createFund(fundName, fundInstallment, maxTicketNumber)
                .send({
                    from: accounts[0],
                    value: fundInstallment * maxTicketNumber
                })
                .then(function (result, error) {
                    if (!error) {
                        swal("Success!", "Added New Trusted Lending Circle", "success")
                            .then(() => {
                                window.location = "/foreman";
                            });
                    } else {
                        swal("Oops", "Something went wrong!", "error");
                    }
                });
        });
    });
});
