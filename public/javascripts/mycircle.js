
$(document).ready(function () {
    ethereum.enable().then(result => {
        account = result[0];
        $.ajax({
            type: 'GET',
            url: '/foreman/myfundcircle',
            contentType: "application/json",
            data: {
                'account': account
            },
            success: function (data) {
                if (data.events.length != 0) {
                    $('#fundName').text(data.events[0].fundName);
                    $('#fundAddress').text(data.events[0].fundAddress);
                    $('#installmentAmount').text(data.events[0].installmentAmount);
                    $('#installment').text(data.events[0].noOfInstallments);

                    $('#subscriberLogLink').attr('href', '/foreman/subscriberLog/' + data.events[0].fundAddress);
                    $('#balanceLogLink').attr('href', '/foreman/balanceLog/' + data.events[0].fundAddress);
                    $('#installmentLogLink').attr('href', '/foreman/installmentLog/' + data.events[0].fundAddress);
                    $('#auctionLogLink').attr('href', '/foreman/auctionLog/' + data.events[0].fundAddress);
                    $('#verificationLogLink').attr('href', '/foreman/verificationLog/' + data.events[0].fundAddress
                     + '/' + data.events[0].noOfInstallments
                     + '/' + data.events[0].installmentAmount
                     );

                } else {
                    $('#fundCard').hide();
                }
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
});
