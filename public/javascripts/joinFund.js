function connectToMetMask() {
    ethereum.enable().then(result => {
        accounts = result[0];
    })
}

function joinFund(fundInstallment) {
    web3.eth.getAccounts().then(function (accounts) {
        myContract.methods.joinFund()
            .send({
                from: accounts[0],
                value: fundInstallment
            })
            .then(function (result, error) {
                if (!error) {
                    swal("Success!", "Successfully joined to this fund", "success")
                        .then(() => {
                            window.location = "/subscriber";
                        });
                } else {
                    swal("Oops", "Something went wrong!", "error");
                }
            });
    });
}

$(function () {
    if (typeof web3 !== 'undefined') {
        web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
    } else {
        // set the provider you want from Web3.providers
        window.web3 = new Web3("http://localhost:8545");
    }

    const contractAddress = "<%= fundAddress %>";
    const contractAbi = [
        {
          "inputs": [
            {
              "internalType": "address",
              "name": "_foreman",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "_securityDeposit",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_fundInstallment",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_maxTickets",
              "type": "uint256"
            }
          ],
          "stateMutability": "payable",
          "type": "constructor"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "timeStamp",
              "type": "uint256"
            },
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "highestBid",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "tickets",
              "type": "uint256"
            }
          ],
          "name": "auctionEventLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "bidStart",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "bidEnd",
              "type": "uint256"
            }
          ],
          "name": "auctionLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "time",
              "type": "uint256"
            },
            {
              "indexed": true,
              "internalType": "address",
              "name": "memberAddress",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "credit",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "debit",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "balanceOfContract",
              "type": "uint256"
            }
          ],
          "name": "balanceLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "timeStamp",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "commission",
              "type": "uint256"
            }
          ],
          "name": "commissionLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "toPay",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "bool",
              "name": "fundClosed",
              "type": "bool"
            },
            {
              "indexed": false,
              "internalType": "bool",
              "name": "fundCanceled",
              "type": "bool"
            }
          ],
          "name": "installmentLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "ticketNumber",
              "type": "uint256"
            },
            {
              "indexed": true,
              "internalType": "address",
              "name": "subscriberAddress",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "paid",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "fine",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "deposit",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "balance",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "bool",
              "name": "isWinner",
              "type": "bool"
            },
            {
              "indexed": false,
              "internalType": "bool",
              "name": "isSubscriber",
              "type": "bool"
            },
            {
              "indexed": true,
              "internalType": "enum Lib.Punishment",
              "name": "due",
              "type": "uint8"
            },
            {
              "indexed": false,
              "internalType": "enum Lib.SubscriberStatus",
              "name": "status",
              "type": "uint8"
            }
          ],
          "name": "subscriberLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "timeStamp",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "address",
              "name": "subscriberAddress",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "enum Lib.Punishment",
              "name": "due",
              "type": "uint8"
            }
          ],
          "name": "verifyLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "installmentIndex",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "timeStamp",
              "type": "uint256"
            },
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "highestBid",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "address",
              "name": "winnerAddress",
              "type": "address"
            }
          ],
          "name": "winnerLog",
          "type": "event"
        },
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "timeStamp",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "amount",
              "type": "uint256"
            },
            {
              "indexed": false,
              "internalType": "address",
              "name": "withdrawer",
              "type": "address"
            },
            {
              "indexed": false,
              "internalType": "uint256",
              "name": "contractBalance",
              "type": "uint256"
            }
          ],
          "name": "withDrawLog",
          "type": "event"
        },
        {
          "inputs": [],
          "name": "joinFund",
          "outputs": [],
          "stateMutability": "payable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "updateInstallmentIndex",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "paySubscription",
          "outputs": [],
          "stateMutability": "payable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_bidTime",
              "type": "uint256"
            }
          ],
          "name": "conductAuction",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_bidAmount",
              "type": "uint256"
            }
          ],
          "name": "bid",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "pickWinner",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "getPrize",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "address",
              "name": "_subscriberAddress",
              "type": "address"
            },
            {
              "internalType": "uint256",
              "name": "_punishment",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_fineDue",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_fineWinner",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_fineDue2",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_fineWinner2",
              "type": "uint256"
            },
            {
              "internalType": "uint256",
              "name": "_interestDividend",
              "type": "uint256"
            }
          ],
          "name": "punishSubscriber",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_commission",
              "type": "uint256"
            }
          ],
          "name": "verifySubsciptions",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "cancelFund",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "withDraw",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "inputs": [],
          "name": "deleteContract",
          "outputs": [],
          "stateMutability": "nonpayable",
          "type": "function"
        }
      ];

    myContract = new web3.eth.Contract(contractAbi, contractAddress);

});
